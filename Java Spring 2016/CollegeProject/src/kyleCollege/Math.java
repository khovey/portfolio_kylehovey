package kyleCollege;

import java.util.ArrayList;

public class Math implements Major
{
    
   //instance variables
   private ArrayList<Course> majorList = new ArrayList<Course>();
      
   public boolean qualifyForMajor(ArrayList<Course> courses){
       for (Course course : courses){
           if (course.mathMajor() == true) {
               majorList.add(course);
            }
        }
       if (majorList.size() >= 3) return true;
       else return false;
    }
    
   //toString
   public String toString(){
       return "Math";
    }
}
