package kyleCollege;

import java.util.ArrayList;

public class CMSC implements Major
{
   //instance variables
   private ArrayList<Course> majorList = new ArrayList<Course>();
   private ArrayList<Course> majorList300 = new ArrayList<Course>();
   
   public boolean qualifyForMajor(ArrayList<Course> courses){
       for (Course course : courses){
           if (course.cmscMajor() == true) {
               majorList.add(course);
            }
           if (course.cmscMajor300() == true){
               majorList300.add(course);
            }
        }
       if (majorList.size() >= 3 && majorList300.size() >= 1) return true;
       else return false;
    }
    
   //toString
   public String toString(){
       return "CMSC";
    } 
}