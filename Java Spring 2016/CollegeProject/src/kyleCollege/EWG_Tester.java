package kyleCollege;

public class EWG_Tester
{
   public static void main(String[] args){
       //creating courses
       Course math150 = new Course("MATH", 150, 4);
       Course cmsc155 = new Course("CMSC", 155, 4);
       EWG_Course math151 = new EWG_Course("MATH", 151, 4, "Ankeny");
       EWG_Course cmsc175 = new EWG_Course("CMSC", 155, 4, "Indianola"); 
       EWG_Course math155 = new EWG_Course("MATH", 155, 4, "West Des Moines");
       
       System.out.println(math150);
       System.out.println(cmsc155);
       System.out.println(math151);
       System.out.println(cmsc175);
       System.out.println(math155);
       
       //creating students
       Student student1 = new Student("Student One", 004, new CMSC());
       Student student2 = new Student("Student Two", 005, new Math());     
       System.out.println(student1);
       System.out.println(student2);
       
       //adding and dropping
       System.out.println(student1.addCourse(math150));
       System.out.println(student1.addCourse(math151));
       System.out.println(student1.dropCourse(math150));
       System.out.println(student1.addCourse(cmsc155));
       System.out.println(student2.addCourse(cmsc155));
       System.out.println(student2.addCourse(math155));
       System.out.println(student2.addCourse(cmsc175));
       
       //printing students
       System.out.println(student1);
       System.out.println(student2);
    }
}
