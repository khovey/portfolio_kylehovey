package kyleCollege;

import java.util.ArrayList;
import java.util.Collections;
import java.util.*;

public class Student
{
    //instance variables
    private String studentName;
    private int studentID;
    private ArrayList<Course> studentCourses = new ArrayList<Course>();
    private boolean sorted;
    public static int nextID = 1;
    Major major;
        
    //constructor
    public Student(String name, int ID, Major studentMajor){
        studentName = name;
        studentID = ID;
        major = studentMajor;
        sorted = true;
    }
    
    //constructor 2
    public Student(String name) {
        studentName = name;
        studentID = nextID;
        nextID++;
    }
    
    //other methods
    public boolean precedesById(Student testStudent){
        if (studentID < testStudent.studentID) {
            return true;
        }
        else return false;
    }
    
    public String addCourse(Course course1){
        sorted = false;
        if ((studentCourses.contains(course1) == false)){
            studentCourses.add(course1);
        }
        String output = "";
        for (Course course: studentCourses){
            output += course + "\n";
        }
        if (studentCourses.size() == 0) return studentName + " is not enrolled in any classes";
        else return studentName + " is currently enrolled in:\n" + output;
    }
    
    public String dropCourse(Course course1){
        if (studentCourses.contains(course1)){
            studentCourses.remove(course1);
        }
        String output = "";
        for (Course course: studentCourses){
            output += course + "\n";
        }
        if (studentCourses.size() == 0) return studentName + " is not enrolled in any classes";
        else return studentName + " is currently enrolled in:\n" + output;
    }
    /*
    public int compareTo(Student other){
        return studentID - other.studentID;
    }
    
    public int compareTo(Student other){
        return other.studentID - studentID;
    }
    
    public int compareTo(Student other){
        return studentName.compareTo(other.studentName);
    }
    */
    public int compareTo(Student other){
        if (studentName.compareTo(other.studentName) == 0)
            return studentID - other.studentID;
        else 
            return studentName.compareTo(other.studentName);
    }
    
    public double avgGrade(Map<Course, Double> grade){
        double avgGrade = 0;
        int count = 0;
        Set<Course> keyset = grade.keySet();
        for (Course key : keyset){
            avgGrade = avgGrade + grade.get(key);
            count++;
        }
        return avgGrade/count;
    }
    
    //toString method
    public String toString(){
        if (sorted == false)
            Collections.sort(studentCourses);
        String courses = "";
            for (Course course: studentCourses){
            courses += "\n" + course;
        }
        String out = "\n" + studentName + "'s Student ID number is " + studentID 
                    + "\n" + major + " major" + 
                    "\nStudent is qualified for major: " + major.qualifyForMajor(studentCourses) +
                    "\nStudents courses: " +  courses;
        if (studentCourses.size() == 0){
           return out + "\nThis student is not enrolled in any classes.";
                }
        if (studentCourses.size() < 3){
           return out + "\nThis is not a full course load.";
                }
        else {
            return out;
                }
            }         
    }