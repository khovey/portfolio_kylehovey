package kyleCollege;

public class WorkStudy_Tester
{
    public static void main(String[] args){
        WorkStudy_Student student1 = new WorkStudy_Student("Student One", 001, new Math());
        WorkStudy_Student student2 = new WorkStudy_Student("Student Two", 002, new CMSC());
        Student student3 = new Student("Student Four", 004, new CMSC());
        Student student4 = new Student("Student Five", 005, new Math());
        
        student1.enterHours(2);
        student2.enterHours(4);
        
        System.out.println(student1);
        System.out.println(student2);
        System.out.println(student3);
        System.out.println(student4);
    }
}
