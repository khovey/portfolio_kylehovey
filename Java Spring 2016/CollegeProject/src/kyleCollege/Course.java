package kyleCollege;

public class Course implements Comparable<Course>
{
    //instance variables
    private String departmentNumber;
    private int courseNumber;
    private int creditHours;
    
    public Course(String department, int course, int credits){
        departmentNumber = department;
        courseNumber = course;
        creditHours = credits;
    }
    
    public int compareTo(Course other){
        if (departmentNumber.compareTo(other.departmentNumber) == 0)
        {
            return courseNumber - other.courseNumber;
        }
        else return departmentNumber.compareTo(other.departmentNumber);
    }
    
    public boolean equals(Object obj){ 
        if (obj instanceof Course){
            Course course1 = (Course) obj;
            return (departmentNumber.equals(course1.departmentNumber)) &&
                    (courseNumber == course1.courseNumber) &&
                    (creditHours == course1.creditHours);
        }
        else return false;
    }
    
    public boolean mathMajor(){
        if (courseNumber >= 200) {
            return true;
        }
        else return false;
    }
    
    public boolean cmscMajor(){
    if (departmentNumber.equals("CMSC")) {
        return true;
        }
        else return false;
    }
    
    public boolean cmscMajor300(){
    if (courseNumber >= 300) {
        return true;
    }
    else return false;
    }
    
    //toString
    public String toString(){
        return "Department Number: " + departmentNumber + " Course Number: " + courseNumber + " Credit Hours: " + creditHours;
    }
}