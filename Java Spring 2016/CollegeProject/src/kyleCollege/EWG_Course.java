package kyleCollege;

public class EWG_Course extends Course
{
   private String location;
   
   public EWG_Course(String department, int course, int credits, String locationEntered){
       super(department, course, credits);
       if (locationEntered.equals("Indianola") || locationEntered.equals("Ankeny") 
            || locationEntered.equals("West Des Moines")) location = locationEntered;
    }
    
   public String toString(){
       return super.toString() + " EWG course in: " + location;
    }
}
