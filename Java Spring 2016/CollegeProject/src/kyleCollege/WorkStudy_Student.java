package kyleCollege;

public class WorkStudy_Student extends Student
{
    private double hoursWorked;
    
    public WorkStudy_Student(String name, int ID, Major studentMajor){
        super(name, ID, studentMajor);
    }
    
    public double enterHours(double hoursWorked){
        this.hoursWorked = hoursWorked;
        return hoursWorked;
    }
    
    public String toString(){
        double amountEarned = hoursWorked * 7.25;
        return super.toString() + "\n This student is a work-study student. \n Amount earned: " 
                + amountEarned;
    }
}
