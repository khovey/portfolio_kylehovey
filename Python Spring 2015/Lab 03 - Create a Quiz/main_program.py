print("Welcome to Kyle Hovey's super fun quiz of death!\r\n ")

print("Question 1:\r\nWhat year did George Washington die?")
print("A. 1774\n2. 1799\nd. 1895")

death_wish = input("Answer at your own risk: ")

if death_wish == "2":
    print("Congratulations, you survived this time.")
    score = 1
elif death_wish == "A":
    print("Nice try, but no.")
    score = 0
elif death_wish == "d":
    print("Nice try, but no.")
    score = 0
else:
    print("That isn't even an option.")
    score = 0
    
print("\r\nQuestion B:\r\nHow many times did Brett get shot in Archer before he died?")
print("H. 5\n1. 1\nn. 8\none. 16")
    
death_wish = input("Answer at your own risk: ")
    
if death_wish == "n":
    print("You survived yet again...")
    score = score + 1
elif death_wish == "H":
    print("Nice try, but no.")
elif death_wish == "1":
    print("Nice try, but no.")
elif death_wish == "one":
    print("Nice try, but no.")
else:
    print("That isn't even an option.")
        
print("\r\nQuestion Three:\r\nHow many people died in the sinking of the Titanic?")
print("R. 2364\ntwo. 1517\nD. 549\nTwo. 1021")
        
death_wish = input("Answer at your own risk: ")
        
if death_wish == "two":
    print("You survived yet again...")
    score = score + 1
elif death_wish == "R":
    print("Nice try, but no.")
elif death_wish == "Two":
    print("Nice try, but no.")
elif death_wish == "D":
    print("Nice try, but no.")
else:
    print("That isn't even an option.")
    
print("\r\nQuestion Z:\r\nHow many Zombies has rick killed in the Walking Dead by the end of season 3?")
print("K. 99\ny. 263\nl. 127\ne. 86")
        
death_wish = input("Answer at your own risk: ")
            
if death_wish == "e":
    print("You survived yet again...")
    score = score + 1
elif death_wish == "K":
    print("Nice try, but no.")
elif death_wish == "y":
    print("Nice try, but no.")
elif death_wish == "l":
    print("Nice try, but no.")
else:
    print("That isn't even an option.")
    
print("\r\nQuestion Q:\r\nWhen will Queen Elizabeth II of England Die?")
print("m. It's probably safe to say she's immortal\ni. February\nI. 2015\nV. 2035")
            
death_wish = input("Answer at your own risk: ")
                
if death_wish == "m":
    print("You survived yet again...")
    score = score + 1
elif death_wish == "i":
    print("Nice try, but no.")
elif death_wish == "I":
    print("Nice try, but no.")
elif death_wish == "V":
    print("Nice try, but no.")
else:
    print("That isn't even an option.")    

score = (score/5)*100
print("\n\rScore=", score, "percent")
if score == 100:
    print ("Dang you're good")
if score == 60 or score == 80:
    print("Good enough I suppose.")
if score == 20 or score == 40:
    print("That was terrible, but I'm a computer, so you're safe.")
if score == 0:
    print("Let's be honest there was never really any threat.")
