import pygame
import random

BLACK = (   0,   0,   0)
WHITE = ( 255, 255, 255)
GREEN_ONE = (   0, 214,   16)
GREEN_TWO = (0, 145,11)
GREEN_THREE = (0, 95, 7)
RED = ( 255,   0,   0)
SKY = (0, 140, 255)
GROUND = (133, 104, 19)
TREE = (69, 42, 0)
GREY = (125, 125, 125)
ORANGE = (255,132,0)
RAIN = (41,35,79)
LAMP = (230,215,16)
BLUE = (0,0,255)
YELLOW = ( 255, 255, 0)

pygame.init()

def background(screen,cloud_list,mouse_x, mouse_y):
    screen.fill(SKY)
    pygame.draw.rect(screen,GROUND,[0,375,800,25])    
    
    for i in range(len(cloud_list)):
        pygame.draw.circle(screen, WHITE, [cloud_list[i][0],cloud_list[i][1]], 25)
        cloud_list[i][0] -= 1
        if cloud_list[i][0] <= -25:
            cloud_list[i][0] = 825  
            
    pygame.draw.circle(screen, RED, [mouse_x, mouse_y], 10, 2)
    pygame.draw.line(screen, RED, [mouse_x-10, mouse_y], [mouse_x+10, mouse_y], 2)
    pygame.draw.line(screen, RED, [mouse_x, mouse_y-10], [mouse_x, mouse_y+10], 2) 
def createCloudlist():
    cloud_list = []
    for i in range(-25,826,25):
        x = i
        y = 50
        cloud_list.append([x,y])
    return cloud_list 
            
def ground(screen):
    pygame.draw.rect(screen,GROUND,[0,375,800,25])