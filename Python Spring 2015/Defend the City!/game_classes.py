import pygame
import random
import math

BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
YELLOW = (255,255,0)
GREEN = (0,255,0)
class constantSprite(pygame.sprite.Sprite):
    def __init__(self,filename):
        super().__init__()
        self.image = pygame.image.load(filename).convert()
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()
        self.health = 100
class Bullet(pygame.sprite.Sprite):
    
    # Craven - Add start position, and goal position to Bullet constructor
    def __init__(self, start_x, start_y, goal_x, goal_y):
        super().__init__()
 
        self.image = pygame.Surface([4,4])
        self.image.fill(RED)
 
        self.rect = self.image.get_rect()

        # Craven - Calculate vector and set initial variables
        self.x = start_x
        self.y = start_y
        
        self.rect.x = self.x
        self.rect.y = self.y
        
        delta_x = goal_x - start_x
        delta_y = goal_y - start_y
    
        angle = math.atan2(delta_y, delta_x)
        speed = 25.0
    
        self.change_x = math.cos(angle) * speed
        self.change_y = math.sin(angle) * speed

 
    def update(self):
        # Craven - self.rect.x and y are automatically rounded to integers
        # here we create other variables that hold the floating point for 
        # more accuracy.
        self.y += self.change_y
        self.x += self.change_x
        self.rect.x = self.x
        self.rect.y = self.y
    def kill(self,screen):
        collision = pygame.image.load("collision.png").convert()
        collision.set_colorkey(BLACK)
        screen.blit(collision, [self.rect.x+50, self.rect.y])
        pygame.sprite.Sprite.kill(self)
        
class planes(pygame.sprite.Sprite):
    
    def __init__(self,filename,start_x ,start_y,screen):
        super().__init__()
        self.image = pygame.image.load(filename).convert()
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()
        self.rect.x = start_x
        self.rect.y = start_y
    def update(self):
        self.rect.x += 5
        if self.rect.x >= 900:
            self.rect.x = -125
        
class EnemyBullet(pygame.sprite.Sprite):
    def __init__(self, filename):
        super().__init__()
        self.image = pygame.image.load(filename).convert()
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()
        
    def update(self):
        self.rect.y += 3
        
        