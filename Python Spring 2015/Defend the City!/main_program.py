import pygame
import newdrawingcode
import game_classes
import random 
# Define some colors
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
GREEN    = (   0, 255,   0)
RED    =   ( 255,   0,   0)
pygame.init()

font = pygame.font.Font(None, 36) 

# Set the width and height of the screen [width, height]
size = (800, 400)
screen = pygame.display.set_mode(size)
 
pygame.display.set_caption("Defend the City!")
 
# Loop until the user clicks the close button.
done = False
game_over = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()
pygame.mouse.set_visible(0)

cloud_list = newdrawingcode.createCloudlist()

explosion = pygame.mixer.Sound("bomb_explosion.wav")
bomb_drop = pygame.mixer.Sound("falling-3.wav")
shot = pygame.mixer.Sound("15.wav")
pygame.mixer.music.load("Blackmoor Colossus Loop.ogg")
pygame.mixer.music.set_endevent(pygame.constants.USEREVENT)
pygame.mixer.music.play()

all_sprites_list = pygame.sprite.Group()
bullet_list = pygame.sprite.Group()
plane_list = pygame.sprite.Group()
enemies = []
enemy_bullet_list = pygame.sprite.Group()
city_list = pygame.sprite.Group()

player = game_classes.constantSprite("20mm.png")
player.rect.x = 675
player.rect.y = 355
all_sprites_list.add(player)

city = pygame.image.load("city.png").convert()
city.set_colorkey(WHITE)
city = game_classes.constantSprite("city.png")
city.rect.x = 706
city.rect.y = 334
all_sprites_list.add(city)
city_list.add(city)



level = 1
score = 0
for i in range(3):
    plane = game_classes.planes("MiG-29.PNG", random.randrange(-300,-200), random.randrange(25,200),screen)
    plane_list.add(plane)
    all_sprites_list.add(plane)
    enemies.append(plane)
# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done = True # Flag that we are done so we exit this loop                    
        
        if event.type == pygame.MOUSEBUTTONDOWN:
                    # Fire a bullet if the user clicks the mouse button
            pos = pygame.mouse.get_pos()
            # Fire a bullet if the user clicks the mouse button
            bullet = game_classes.Bullet(player.rect.x+10, player.rect.y+10, pos[0], pos[1])
                    # Add the bullet to the lists
            all_sprites_list.add(bullet)
            bullet_list.add(bullet)        
            shot.play()
        
        
        elif event.type == pygame.constants.USEREVENT:
            pygame.mixer.music.load("Blackmoor Colossus Loop.ogg")
            pygame.mixer.music.play()
        
    if not game_over:
        all_sprites_list.update()            
    
    # --- Game logic
    pos = pygame.mouse.get_pos()
    mouse_x = pos[0]
    mouse_y = pos[1]


    for count in range(len(enemies)):

             
        enemy_bullet_chance = random.randrange(0, 10)
        if enemy_bullet_chance == 0 and enemies[count].rect.x > 725:
            enemy_bullet = game_classes.EnemyBullet("bomb_4.png")
            enemy_bullet.rect.x = enemies[count].rect.x
            enemy_bullet.rect.y = enemies[count].rect.y
            all_sprites_list.add(enemy_bullet)
            enemy_bullet_list.add(enemy_bullet)  
            bomb_drop.play()
    
    for bullet in enemy_bullet_list: 
        
        player_hit_list = pygame.sprite.spritecollide(bullet, city_list, False)
        
        for block in player_hit_list:
            city.health -= 10
            enemy_bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
            explosion.play()
            if city.health <= 0:
                game_over = True
            
        if enemy_bullet.rect.y >= 322:
            enemy_bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
                
    
    
    for bullet in bullet_list: 
        
        block_hit_list = pygame.sprite.spritecollide(bullet, plane_list, False)
        
        for block in block_hit_list:
            plane_list.remove(block)
            all_sprites_list.remove(block)
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
            enemies.remove(block)
            score += 1
            
        if bullet.rect.y < 0:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
            
        if bullet.rect.x < 0:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)    

    if len(plane_list) == 0:
        level = level + 1 
        for i in range(level + 5):
            plane = game_classes.planes("MiG-29.PNG", random.randrange(-500,-200), random.randrange(25,200),screen)
            plane_list.add(plane)
            all_sprites_list.add(plane)
            enemies.append(plane)
           
    
    # --- Drawing code
    newdrawingcode.background(screen, cloud_list,mouse_x,mouse_y)
    pygame.draw.rect(screen, GREEN, [700,375,city.health,10])
    text = font.render("Score: "+str(score), True, BLACK)
    screen.blit(text, [10, 350])
    text = font.render("Level: "+str(level), True, BLACK)
    screen.blit(text, [10, 40])    
    all_sprites_list.draw(screen)
    if game_over:
            screen.fill(RED)
            text = font.render("Game Over", True, BLACK)
            screen.blit(text, [300, 200])
            text = font.render("Score: "+str(score), True, BLACK)
            screen.blit(text, [300, 250])  
            
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
    clock.tick(60)

pygame.quit()