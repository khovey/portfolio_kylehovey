#1
def min(a,b,c):
    if a <= b and a <= c:
        return a
    elif b <= a and b <= c:
        return b
    else:
        return c
#2    
def box(h,w):
    for i in range(h):
        for i in range(w):
            print("*", end = "")
        print()

#3        
def find(list,key):
    for position in range(len(list)):
        if list[position] == key:
            print("Found", list[position], "at position", position)

# 4 Functions            
import random

def create_list(size):
    list = []
    for list_length in range(size):
        list.append(random.randrange(1,7))
    return list

def count_list(list, key):
    key_in_list = 0
    for position in range(len(list)):
        if list[position] == key:
            key_in_list += 1
    return key_in_list

def average_list(list):
    list_total = 0
    for pos in range(len(list)):
        list_total += list[pos]
    list_avg = list_total/len(list)
    return list_avg

# 4 Main Program

list = create_list(1000)
print("There are ",count_list(list, 1), "ones")
print("There are ",count_list(list, 2),"twos")
print("There are ",count_list(list, 3),"threes")
print("There are ",count_list(list, 4),"fours")
print("There are ",count_list(list, 5),"fives")
print("There are ",count_list(list, 6),"sixes")
print("The average of the list is ", average_list(list))