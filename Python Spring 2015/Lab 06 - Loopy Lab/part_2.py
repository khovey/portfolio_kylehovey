done = False
while not done:
    n = int(input("Box size:"))
    for i in range(n*2):
        print("o", end="")
    print()
    for i in range(n-2):
        print("o",end="")
        for j in range((n*2)-2):
            print(" ", end="")
        print("o",end="")
        print()
    for i in range(n*2):
        print("o", end="")
    print()
    q = str(input("Make another box? y/n"))
    if q == "n":
        done = True
