"""Lab 14
Collecting Sprites
Kyle Hovey"""

import pygame
import random
 
# Define some colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
BLUE  = (  0,   0, 255)  
GREEN = (  0, 255,   0) 
 
class Block(pygame.sprite.Sprite):
    
    def __init__(self,filename):
        
        super().__init__()
 
        self.image = pygame.image.load(filename).convert()
        self.image.set_colorkey(WHITE)
 
        self.rect = self.image.get_rect()

class Player(pygame.sprite.Sprite):
    """ The class is the player-controlled sprite. """
 
    # -- Methods
    def __init__(self, x, y):
        """Constructor function"""
        # Call the parent's constructor
        super().__init__()
 
        # Set height, width
        self.image = pygame.Surface([15, 15])
        self.image.fill(BLUE)
 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
 
        # -- Attributes
        # Set speed vector
        self.change_x = 0
        self.change_y = 0
 
 
    def changespeed(self, x, y):
        """ Change the speed of the player"""
        self.change_x += x
        self.change_y += y
 
 
    def update(self):
        """ Find a new position for the player"""
        self.rect.x += self.change_x
        self.rect.y += self.change_y
        if player.rect.x >= 686:
            player.rect.x = 685
            wall_sound.play()
        if player.rect.x <= -1:
            player.rect.x = 0
            wall_sound.play()
        if player.rect.y >= 386:
            player.rect.y = 385
            wall_sound.play()
        if player.rect.y <= -1:
            player.rect.y = 0
            wall_sound.play()

# Initialize Pygame
pygame.init()
 
# Set the height and width of the screen
screen_width = 700
screen_height = 400
screen = pygame.display.set_mode([screen_width, screen_height])
 

good_block_list = pygame.sprite.Group()
bad_block_list = pygame.sprite.Group()
all_sprites_list = pygame.sprite.Group()
 
for i in range(50):
    # This represents a block
    block = Block("coin.png")
 
    # Set a random location for the block
    block.rect.x = random.randrange(screen_width - 20)
    block.rect.y = random.randrange(screen_height - 20)
 
    # Add the block to the list of objects
    good_block_list.add(block)
    all_sprites_list.add(block)

for i in range(50):
    # This represents a block
    block = Block("bomb_4.png")
 
    # Set a random location for the block
    block.rect.x = random.randrange(screen_width - 20)
    block.rect.y = random.randrange(screen_height - 20)
 
    # Add the block to the list of objects
    bad_block_list.add(block)
    all_sprites_list.add(block)

#Create Player Block
player = Player(350,200)
all_sprites_list.add(player)
 
# Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()
 
score = 0

pygame.mouse.set_visible(False)

font = pygame.font.SysFont("Calibri", 25, True, False)

wall_sound = pygame.mixer.Sound("squeak.wav")
good_sound = pygame.mixer.Sound("Pickup_Coin.wav")
bad_sound = pygame.mixer.Sound("Explosion2.wav")
# -------- Main Program Loop -----------
while not done:
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT: 
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                player.changespeed(-3, 0)
            elif event.key == pygame.K_RIGHT:
                player.changespeed(3, 0)
            elif event.key == pygame.K_UP:
                player.changespeed(0, -3)
            elif event.key == pygame.K_DOWN:
                player.changespeed(0, 3)
     
            # Reset speed when key goes up
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                player.changespeed(3, 0)
            elif event.key == pygame.K_RIGHT:
                player.changespeed(-3, 0)
            elif event.key == pygame.K_UP:
                player.changespeed(0, 3)
            elif event.key == pygame.K_DOWN:
                player.changespeed(0, -3)    
    
    
    
    # Clear the screen
    screen.fill(WHITE)
    
 
    # Fetch the x and y out of the list,
       # just like we'd fetch letters out of a string.
    # Set the player object to the mouse location
    
    # See if the player block has collided with anything good.
    good_blocks_hit_list = pygame.sprite.spritecollide(player, good_block_list, True)
 
    # Check the list of collisions.
    for block in good_blocks_hit_list:
        score += 1
        good_sound.play()
        
    
    # See if the player block has collided with anything bad.
    bad_blocks_hit_list = pygame.sprite.spritecollide(player, bad_block_list, True)
 
    # Check the list of collisions.
    for block in bad_blocks_hit_list:
        score -= 1
        bad_sound.play() 
        
    # Draw all the spites
    all_sprites_list.draw(screen)
    all_sprites_list.update()
    
    text = font.render("Score: " + str(score), True, BLACK)
    screen.blit(text, [0,350])
    
    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
 
    # Limit to 60 frames per second
    clock.tick(60)
 
pygame.quit()