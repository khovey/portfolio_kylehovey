import pygame
import random

# Define some colors
BLACK = (   0,   0,   0)
WHITE = ( 255, 255, 255)
GREEN_ONE = (   0, 214,   16)
GREEN_TWO = (0, 145,11)
GREEN_THREE = (0, 95, 7)
RED = ( 255,   0,   0)
SKY = (20, 0, 122)
WOOD = (133, 104, 19)
TREE = (69, 42, 0)
GREY = (125, 125, 125)
ORANGE = (255,132,0)
RAIN = (41,35,79)

pygame.init()

# Clouds
cloud_list = []

for i in range(-25,726,25):
    x = i
    y = 75
    cloud_list.append([x,y])
    
# Rain
rain_list = []

for i in range(400):
    top_x = random.randrange(0,700)
    top_y = random.randrange(75,500)
    bottom_x = top_x
    bottom_y = top_y + 25
    rain_list.append([top_x,top_y,bottom_x,bottom_y])

# Stars

star_list = []

for i in range(50):
    star_x = random.randrange(0, 700)
    star_y = random.randrange(0, 175)
    star_list.append([star_x,star_y])

# Set the width and height of the screen [width, height]
size = (700, 500)
screen = pygame.display.set_mode(size)
 
pygame.display.set_caption("My Game")
 
# Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()
 
# Main Program Loop
while not done:
    # Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
 
    # Drawing code

    # Background
    screen.fill(SKY)

    # Grass
    pygame.draw.rect(screen, GREEN_ONE, [0,300,700,200])
    pygame.draw.rect(screen, GREEN_TWO, [0,250,700,50])
    pygame.draw.rect(screen, GREEN_THREE, [0,200,700,50])
    
    # Moon
    pygame.draw.ellipse(screen,WHITE, [500,20,50,50])

    # Stars
    for i in range(len(star_list)):
        pygame.draw.circle(screen,WHITE,star_list[i], 2)

    # Lightning
    lightning_chance = random.randrange(100)
    if lightning_chance == False:
        lightning_x = random.randrange(0,700)
        pygame.draw.line(screen, WHITE, [lightning_x,100],[lightning_x + 25,125],10)
        pygame.draw.line(screen,WHITE, [lightning_x + 25,125],[lightning_x,150],10)
        pygame.draw.line(screen,WHITE, [lightning_x,150],[lightning_x,200],10)
        pygame.draw.line(screen, RED, [lightning_x,200],[lightning_x - 10,195],2)
        pygame.draw.line(screen, ORANGE, [lightning_x,200],[lightning_x - 12,198],2)
        pygame.draw.line(screen, RED, [lightning_x,200],[lightning_x + 10,195],2)
        pygame.draw.line(screen, ORANGE, [lightning_x,200],[lightning_x + 12,198],2)     

    # Trees
    for x_offset in range(50,700,150):
        pygame.draw.rect(screen,TREE,[x_offset,150,15,75])
        pygame.draw.ellipse(screen,GREEN_TWO,[x_offset - 20,125,55,55])
        pygame.draw.rect(screen,TREE,[x_offset + 75,175,20,100])
        pygame.draw.ellipse(screen,GREEN_TWO,[x_offset + 50,145,75,75])
    # Lamp
    for x_offset in range(50,700, 575):
        pygame.draw.rect(screen, BLACK, [x_offset,225,25,250])
        pygame.draw.polygon(screen,BLACK,[[x_offset-10,215], [x_offset+35,215], [x_offset+12.5,255]])
        pygame.draw.circle(screen,WHITE,[x_offset+12,200],30)

    # Rain
    for i in range(len(rain_list)):
        pygame.draw.line(screen,RAIN,[rain_list[i][0],rain_list[i][1]],[rain_list[i][2],rain_list[i][3]],2)
        rain_list[i][1] += 10
        rain_list[i][3] += 10
        if rain_list[i][1] > 500:
            top_y = 75
            rain_list[i][1] = top_y
            rain_list[i][3] = top_y + 25
            top_x = random.randrange(0,700)
            rain_list[i][0] = top_x
            rain_list[i][2] = top_x
    # Clouds
    pygame.draw.rect(screen, GREY, [0,75,700,25])
    
    for i in range(len(cloud_list)):
        pygame.draw.circle(screen, GREY, [cloud_list[i][0],cloud_list[i][1]], 25)
        cloud_list[i][0] += 1
        if cloud_list[i][0] >= 725:
            cloud_list[i][0] = -25    

    # Deck
    pygame.draw.rect(screen, WOOD, [0,400,700,50],0)
    for x_offset in range(25, 700, 100):
        pygame.draw.rect(screen, WOOD, [x_offset,450,50,100],0)
    
    # Gun
    pygame.draw.line(screen, GREY, [350,375],[375,485], 10)
    pygame.draw.line(screen,TREE, [348,400],[368,485],8)
    pygame.draw.line(screen,TREE, [373,485],[380,505],18)
    pygame.draw.line(screen,BLACK, [355,487],[365,485],3)
    
    pygame.display.flip()
    
    clock.tick(20)
 
pygame.quit()