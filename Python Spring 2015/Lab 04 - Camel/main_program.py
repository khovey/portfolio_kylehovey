import random

print("Welcome to Indiana Jones and the Golden Camel of Tenbuktu")
print("You have stolen a camel from the natives in order to")
print("make your way across the desert.")
print("Outrun the natives and survive the heat of the desert to win!\n\n")

miles_traveled = 0
thirst = 0
camel_tiredness = 0
native_distance = -20
drinks_left = 3
natives = 5

done = False

while not done:
    print("\nA. Drink from your canteen")
    print("B. Ahead moderate speed.")
    print("C. Ahead full speed.")
    print("D. Stop for the night.")
    print("E. Use your whip against the natives.")
    print("F. Status Check.")
    print("Q. Quit.")

    choice = input("Your Choice? ")
    
   #main program loop
    if choice.upper() == "Q":
        done = True
        print("Goodbye!")
    
    elif choice.upper() == "F":
        print("Miles Traveled: ", miles_traveled)
        print("Drinks in Canteen: ", drinks_left)
        print("The natives are ", miles_traveled - native_distance, " miles behind you.")

    elif choice.upper() == "E":
        hit = random.randrange(2)
        if hit == 0:
            print("You didn't hit a native!")
            native_distance = native_distance + random.randrange(2, 5)
            thirst = thirst + 1        
        else:
            print("You killed a native!")
            natives = natives - 1
            thirst = thirst + 1
            if natives == 0:
                print("Congratulations you killed all the natives!\nYou win!")
                done = True

    elif choice.upper() == "D":
        print("The golden camel is greatful.")
        camel_tiredness = 0
        native_distance = native_distance + random.randrange(7, 15)

    elif choice.upper() == "C":
        miles_traveled = miles_traveled + random.randrange(10, 21)
        thirst = thirst + 1
        camel_tiredness = camel_tiredness + random.randrange(1, 3)
        native_distance = native_distance + random.randrange(7, 15)

    elif choice.upper() == "B":
        miles_traveled = miles_traveled + random.randrange(5, 13)
        camel_tiredness = camel_tiredness + random.randrange(0, 2)
        thirst = thirst + 1
        native_distance = native_distance + random.randrange(7, 14)

    elif choice.upper() == "A":
        if drinks_left > 0:
            thirst = 0
            drinks_left = drinks_left - 1
            print("You have ", drinks_left, "drinks left")
        if drinks_left == 0:
            print("You are out of drinks!")
    
    #Oasis
    chance = random.randrange(20)
    if chance == 1 and choice.upper() != "F" and choice.upper() != "E":
        print("You found an oasis!")
        thirst = 0
        drinks_left = 3
        camel_tiredness = 0
           
    # Win or lose conditions
    if thirst >= 4 and thirst < 6 and chance != 1:
        print("You are thirsty!")
    if thirst >= 6 and chance != 1:
        print("You died of thirst")
        done = True
    if camel_tiredness >= 5 and camel_tiredness < 8 and chance != 1:
        print("The golden camel is getting tired!")
    if camel_tiredness >= 8 and chance != 1:
        print("The golden camel has died")
        done = True
    if miles_traveled - native_distance <= 15 and miles_traveled - native_distance >= 0:
        print("The natives are getting close!")
    if miles_traveled - native_distance <= 0:
        print("You were captured by the natives!")
        done = True
    if miles_traveled >= 200:
        print("You escaped the desert!\nYou win!")
        done = True