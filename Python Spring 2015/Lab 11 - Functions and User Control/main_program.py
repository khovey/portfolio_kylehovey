import pygame
import random

# Define some colors
BLACK = (   0,   0,   0)
WHITE = ( 255, 255, 255)
GREEN_ONE = (   0, 214,   16)
GREEN_TWO = (0, 145,11)
GREEN_THREE = (0, 95, 7)
RED = ( 255,   0,   0)
SKY = (20, 0, 122)
WOOD = (133, 104, 19)
TREE = (69, 42, 0)
GREY = (125, 125, 125)
ORANGE = (255,132,0)
RAIN = (41,35,79)
LAMP = (230,215,16)
BLUE = (0,0,255)
HAND     = ( 245, 230, 162)


pygame.init()

def draw_crosshairs(screen, crosshair_x, crosshair_y):
    pygame.draw.circle(screen, RED, [crosshair_x, crosshair_y], 10, 2)
    pygame.draw.line(screen, RED, [crosshair_x-10, crosshair_y], [crosshair_x+10, crosshair_y], 2)
    pygame.draw.line(screen, RED, [crosshair_x, crosshair_y-10], [crosshair_x, crosshair_y+10], 2)

def draw_player(screen, player_x, player_y):
    pygame.draw.line(screen, BLUE, [player_x - 25, player_y], [player_x - 5, player_y - 75],20)
    pygame.draw.line(screen, BLUE, [player_x + 25, player_y], [player_x + 5, player_y - 75],20)
    pygame.draw.circle(screen, BLACK, [player_x - 5, player_y - 80], 11 )
    pygame.draw.circle(screen, HAND, [player_x - 5, player_y - 80], 10 )
    pygame.draw.circle(screen, BLACK, [player_x + 5, player_y - 80], 11 )
    pygame.draw.circle(screen, HAND, [player_x + 5, player_y - 80], 10 )
    pygame.draw.line(screen, BLACK, [player_x,player_y - 125],[player_x,player_y - 80],10)
    pygame.draw.line(screen, BLACK, [player_x,player_y - 130],[player_x,player_y - 80],5)

# Clouds
cloud_list = []

for i in range(-25,726,25):
    x = i
    y = 75
    cloud_list.append([x,y])
    
# Rain
rain_list = []

for i in range(400):
    top_x = random.randrange(0,700)
    top_y = random.randrange(75,500)
    bottom_x = top_x
    bottom_y = top_y + 25
    rain_list.append([top_x,top_y,bottom_x,bottom_y])

# Stars

star_list = []

for i in range(50):
    star_x = random.randrange(0, 700)
    star_y = random.randrange(0, 175)
    star_list.append([star_x,star_y])

# Set the width and height of the screen [width, height]
size = (700, 500)
screen = pygame.display.set_mode(size)
 
pygame.display.set_caption("My Game")
 
# Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Hide the mouse
pygame.mouse.set_visible(0)

player_speed = 0
player_posx = 350
player_posy = 500

# Main Program Loop
while not done:
    # Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_a:
                player_speed = 0
            elif event.key == pygame.K_d:
                player_speed = 0
        
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                player_speed = -5
            elif event.key == pygame.K_d:
                player_speed = 5
        
    
    # game logic
    
    pos = pygame.mouse.get_pos()
    crosshair_x = pos[0]
    crosshair_y = pos[1]    

    player_posx += player_speed
    if player_posx > 700:
        player_posx = 700
    if player_posx < 0:
        player_posx = 0
        
    # Drawing code

    # Background
    screen.fill(SKY)

    # Grass
    pygame.draw.rect(screen, GREEN_ONE, [0,300,700,200])
    pygame.draw.rect(screen, GREEN_TWO, [0,250,700,50])
    pygame.draw.rect(screen, GREEN_THREE, [0,200,700,50])
    
    # Moon
    pygame.draw.ellipse(screen,WHITE, [500,20,50,50])

    # Stars
    for i in range(len(star_list)):
        pygame.draw.circle(screen,WHITE,star_list[i], 2)

    # Lightning
    lightning_chance = random.randrange(100)
    if lightning_chance == False:
        lightning_x = random.randrange(0,700)
        pygame.draw.line(screen, WHITE, [lightning_x,100],[lightning_x + 25,125],10)
        pygame.draw.line(screen,WHITE, [lightning_x + 25,125],[lightning_x,150],10)
        pygame.draw.line(screen,WHITE, [lightning_x,150],[lightning_x,200],10)
        pygame.draw.line(screen, RED, [lightning_x,200],[lightning_x - 10,195],2)
        pygame.draw.line(screen, ORANGE, [lightning_x,200],[lightning_x - 12,198],2)
        pygame.draw.line(screen, RED, [lightning_x,200],[lightning_x + 10,195],2)
        pygame.draw.line(screen, ORANGE, [lightning_x,200],[lightning_x + 12,198],2)     

    # Trees
    for x_offset in range(50,700,150):
        pygame.draw.rect(screen,TREE,[x_offset,150,15,75])
        pygame.draw.ellipse(screen,GREEN_TWO,[x_offset - 20,125,55,55])
        pygame.draw.rect(screen,TREE,[x_offset + 75,175,20,100])
        pygame.draw.ellipse(screen,GREEN_TWO,[x_offset + 50,145,75,75])
    # Lamp
    for x_offset in range(50,700, 575):
        pygame.draw.rect(screen, BLACK, [x_offset,225,25,250])
        pygame.draw.polygon(screen,BLACK,[[x_offset-10,215], [x_offset+35,215], [x_offset+12.5,255]])
        pygame.draw.circle(screen,LAMP,[x_offset+12,200],30)

    # Rain
    for i in range(len(rain_list)):
        pygame.draw.line(screen,RAIN,[rain_list[i][0],rain_list[i][1]],[rain_list[i][2],rain_list[i][3]],2)
        rain_list[i][1] += 10
        rain_list[i][3] += 10
        if rain_list[i][1] > 500:
            top_y = 75
            rain_list[i][1] = top_y
            rain_list[i][3] = top_y + 25
            top_x = random.randrange(0,700)
            rain_list[i][0] = top_x
            rain_list[i][2] = top_x
    # Clouds
    pygame.draw.rect(screen, GREY, [0,75,700,25])
    
    for i in range(len(cloud_list)):
        pygame.draw.circle(screen, GREY, [cloud_list[i][0],cloud_list[i][1]], 25)
        cloud_list[i][0] += 1
        if cloud_list[i][0] >= 725:
            cloud_list[i][0] = -25    

    # Deck
    pygame.draw.rect(screen, WOOD, [0,400,700,50],0)
    for x_offset in range(25, 700, 100):
        pygame.draw.rect(screen, WOOD, [x_offset,450,50,100],0)
    
    draw_crosshairs(screen, crosshair_x, crosshair_y)
    draw_player(screen, player_posx, player_posy)
    
    pygame.display.flip()
    
    clock.tick(60)
 
pygame.quit()