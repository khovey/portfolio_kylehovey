﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EnhancedCalculator
{
    public partial class frmEnhancedCalculator : Form
    {
        string expression = "";
        string[] tokens;
        int numberTokens = 0;
        bool error = false;

        public frmEnhancedCalculator()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 1;
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 2;
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 3;
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 4;
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 5;
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 6;
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 7;
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 8;
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 9;
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtExpression.Text += 0;
        }

        private void btnDec_Click(object sender, EventArgs e)
        {
            txtExpression.Text += ".";
        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            txtExpression.Text += "/";
        }

        private void btnMultiply_Click(object sender, EventArgs e)
        {
            txtExpression.Text += "*";
        }

        private void btnSubtract_Click(object sender, EventArgs e)
        {
            txtExpression.Text += "-";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtExpression.Text += "+";
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            txtExpression.Text += "(";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            txtExpression.Text += ")";
        }

        private void btnEquals_Click(object sender, EventArgs e)
        {
            expression = txtExpression.Text;
            txtDebugging.Text = expression;
            tokens = Tokenize(expression);
            if (!error) CheckCorrectness(tokens);
            if (!error) txtResult.Text = Evaluate(in2Post(tokens)).ToString();
            error = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtDebugging.Clear();
            txtExpression.Clear();
            txtResult.Clear();
            expression = "";
        }

        private string[] Tokenize(string expression)
        {
            expression.Trim();

            tokens = new string[expression.Length];

            int tokensIndex = 0;
            string token;
            int i = 0;

            while (i < expression.Length && !error)
            {
                if (expression[i] == '+' || expression[i] == '-' ||
                    expression[i] == '/' || expression[i] == '*' ||
                    expression[i] == '(' || expression[i] == ')')
                {
                    tokens[tokensIndex] = expression[i].ToString();
                    tokensIndex++;
                    i++;
                }

                else if (expression[i] >= '0' && expression[i] <= '9' || expression[i] == '.')
                {
                    token = "";
                    bool dec = false;
                    while (i < expression.Length &&
                        ((expression[i] >= '0' && expression[i] <= '9') ||
                        (expression[i] == '.' && dec == false)))
                    {
                        if (expression[i] == '.') dec = true;
                        if (expression[i] == '.' && token == "") token += "0" + expression[i];
                        else token += expression[i];
                        i++;
                    }

                    tokens[tokensIndex] = token;
                    tokensIndex++;
                }

                else if (expression[i] == ' ')
                {
                    i++;
                }

                else
                {
                    error = true;
                    MessageBox.Show("Expression contains invalid characters", "Error");
                    txtExpression.Text = "";
                    txtDebugging.Text = "";
                    txtResult.Text = "";
                    expression = "";
                }
            }
            numberTokens = tokensIndex;

            txtDebugging.Text = "";
            for (int j = 0; j < numberTokens; j++)
            {
                txtDebugging.Text += tokens[j] + "|";
            }

            return tokens;
        }

        private void CheckCorrectness(string[] tokens)
        {
            int match = 0;
            string message = "";
            for (int i = 0; i < numberTokens; i += 1)
            {
                //check for two adjacent numbers
                if (i < numberTokens - 1 && tokens[i][0] >= '0' && tokens[i][0] <= '9' && tokens[i + 1][0] >= '0' && tokens[i + 1][0] <= '9')
                {
                    error = true;
                    message = "There are two adjacent numbers.\n";
                    break;
                }
                //check for two adjacent operations
                else if (i < numberTokens-1 && (tokens[i] == "+" || tokens[i] == "-" || tokens[i] == "*" || tokens[i] == "/") &&
                    (tokens[i + 1] == "+" || tokens[i + 1] == "-" || tokens[i + 1] == "*" || tokens[i + 1] == "/"))
                {
                    error = true;
                    message = "There are two adjacent operators.\n";
                    break;
                }
                //check for '(+'
                else if (i < numberTokens-1 && tokens[i] == "(" && 
                    (tokens[i + 1] == "+" || tokens[i + 1] == "-" || tokens[i + 1] == "*" || tokens[i + 1] == "/"))
                {
                    error = true;
                    message = "There is a parentheses adjacent with an operator";
                    break;
                }
                else if (i < numberTokens - 1 && (tokens[i] == "+" || tokens[i] == "-" || tokens[i] == "*" || tokens[i] == "/")
                    && tokens[i + 1] == ")")
                {
                    error = true;
                    message = "There is a parentheses adjacent with an operator";
                    break;
                }
                //check for adjacent parentheses
                else if (i < numberTokens - 1 && (tokens[i] == ")" || tokens[i] == "(") && (tokens[i+1] == ")" || tokens[i+1] == "("))
                {
                    error = true;
                    message = "There are adjacent parentheses";
                    break;
                }

                //increment count for matching parentheses
                else if (tokens[i] == "(") match++;
                else if (tokens[i] == ")")
                {
                    match--;
                    if (match < 0)
                    {
                        error = true;
                        message = "Open and close parentheses do not match.";
                        break;
                    }
                }
            }
            //check count of matching parentheses
            if (match != 0 && !error)
            {
                error = true;
                message += "Open and close parentheses do not match.\n";
            }
            //check for operation or close parentheses at beginning
            if (tokens[0] == "+" || tokens[0] == "-" || tokens[0] == "*" ||
                tokens[0] == "/" || tokens[0] == ")")
            {
                error = true;
                message += "Start of expression invalid.\n";
            }
            //check for operation or open parentheses at end
            if (tokens[numberTokens - 1] == "+" || tokens[numberTokens - 1] == "-" ||
                tokens[numberTokens - 1] == "*" || tokens[numberTokens - 1] == "/" ||
                tokens[numberTokens - 1] == "(")
            {
                error = true;
                message += "End of expression invalid.\n";
            }

            if (error)
            {
                MessageBox.Show("Invalid expression.\n\n" + message, "Error");
                txtExpression.Text = "";
                txtResult.Text = "";
                txtDebugging.Text = "";
                expression = "";
            }
        }

        public static Stack<String> in2Post(String[] tokens)
        {
            Stack<String> postFixrev = new Stack<String>();
            Stack<String> ops = new Stack<String>();
            Stack<String> postFix = new Stack<String>();
            int i;


            String el;
            String op;
            int size = tokens.Length;
            char tokenType;
            for (i = 0; i < size && tokens[i] != null; i++)
            {
                el = tokens[i];
                tokenType = el[0];

                if (el.Length > 1) tokenType = 'd';    // digits
                if (tokenType >= '0' && tokenType <= '9') tokenType = 'd';

                switch (tokenType)
                {
                    case 'd':
                        postFixrev.Push(el);
                        break;

                    case '(':
                        ops.Push(el);
                        break;

                    case ')':
                        op = ops.Pop();  // if ')' pop from ops and push
                                         // onto postFixrev stack
                                         // all operators until
                                         // encountering '(', then pop‘('

                        while (op != "(")
                        {
                            postFixrev.Push(op);
                            op = ops.Pop();
                        }
                        break;

                    case '*':                      // if * or / pop from ops and push
                                                   // onto postFixrev stack all * and /
                    case '/':                      // down to but not ncluding the topmost
                                                   // '('    '+' or '-', or to the
                                                   // bottom of stack,
                                                   // then push the new symbol * or /
                                                   // onto ops
                        while (!(ops.Count == 0)
                               && ops.Peek()!= "("
                               && ops.Peek() != "+"
                               && ops.Peek() != "-"
                               && ops.Peek() != "~")   // unary minus     
                        {
                            op = ops.Pop();
                            postFixrev.Push(op);
                        }
                        ops.Push(el);                    // push the new symbol
                        break;


                    case '+':                 // pop from ops and push onto
                    case '-':                 // postFixrev stack all operators
                                              // from the top down to, but 
                                              // not including the topmost '('
                                              // or to the bottom of the stack
                                              // then push the new symbol -, +
                        while (!(ops.Count == 0)
                            && ops.Peek()!="(")
                        {
                            op = ops.Pop();             //  '+' or '-'
                            postFixrev.Push(op);
                        }
                        ops.Push(el);                 // push the new symbol
                        break;
                }   // end switch statement  
            } // end for loop
            while (!(ops.Count==0))      // write the remaining operators
            {
                op = ops.Pop();
                postFixrev.Push(op);
            }

            while (!(postFixrev.Count==0))        // transfer in reverse order
            {
                el = postFixrev.Pop();
                postFix.Push(el);
            }

            while (!(postFix.Count == 0))        // print the contents of the stack
            {
                el = postFix.Pop();                 //  NOTE: after the stack is printed
                                                    //  it will be empty!
                postFixrev.Push(el);
            }
            while (!(postFixrev.Count == 0))        // transfer in reverse order
            {
                el = postFixrev.Pop();
                postFix.Push(el);
            }

            return postFix;
        }

        public static decimal Evaluate(Stack<String> postFix)
        {

            Stack<Decimal> compute = new Stack<Decimal>();
            String temp;
            decimal op1, op2;
            char operation;
            decimal decVal;
            while (!(postFix.Count == 0))
            {
                temp = postFix.Pop();

                if (temp.Length > 1 || (temp[0] >= '0' && temp[0] <= '9'))
                {
                    decVal = Decimal.Parse(temp);
                    compute.Push(decVal);
                }
                else
                {
                    op2 = compute.Pop();
                    op1 = compute.Pop();

                    operation = temp[0];
                    switch (operation)
                    {
                        case '-':
                            decVal = op1 - op2;
                            compute.Push(decVal);
                            break;
                        case '+':
                            decVal = op1 + op2;
                            compute.Push(decVal);
                            break;
                        case '*':
                            decVal = op1 * op2;
                            compute.Push(decVal);
                            break;
                        case '/':
                            decVal = op1 / op2;
                            compute.Push(decVal);
                            break;
                    } // switch

                } // end else operator
            } // end postfix not empty
            decVal = compute.Pop();
            return decVal;
        }

    }
}
